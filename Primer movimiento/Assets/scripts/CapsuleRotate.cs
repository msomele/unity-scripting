﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotate : MonoBehaviour {

    public float rotationDegrees = 5f;
    // Use this for initialization
    void Start()
    {
        transform.Rotate(new Vector3(0, rotationDegrees * Time.deltaTime, 0)); ;
        Debug.Log("Moving from Start Event");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, -rotationDegrees * Time.deltaTime, 0));
        Debug.Log("Moving from Update Event");
    }
}
