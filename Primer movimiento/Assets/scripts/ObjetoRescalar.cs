﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetoRescalar : MonoBehaviour {
    public Vector3 escala;
    float x;
    float y;
    float z;
    float xR;
    float yR;
    float zR;
    Vector3 gen;
    public bool RandomScale = false;
	// Use this for initialization
	void Start () {
        var inicio = GetComponent<Transform>();
        Vector3 ppo = inicio.localScale;
        gen = ppo;

    }
	
	// Update is called once per frame
	void Update () {
        var tr = GetComponent<Transform>();
        x = 0.2f * Time.deltaTime;
        y = 0.2f * Time.deltaTime;
        z = 0.2f * Time.deltaTime;
        escala = tr.localScale;

        xR = Random.Range(0,5) * Time.deltaTime;
        yR = Random.Range(0,10) * Time.deltaTime;
        zR = Random.Range(0,3) * Time.deltaTime;

        if (RandomScale == false)
        {
            if (Input.GetButton("Scale"))
            {
                transform.localScale += new Vector3(x, y, z);
            }
            else
            {
                transform.localScale = gen;
            }
        }
        else
        {
            if (Input.GetButton("Scale"))
            {
                transform.localScale += new Vector3(xR, yR, zR);
            }
            else
            {
                transform.localScale = gen;
            }
        }
	}
}
