﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambioColor : MonoBehaviour
{

    public Color colorcamb;
    private Renderer colorplayer;
    public bool cambio;

    // Use this for initialization
    void Start()
    {
        cambio = true;
        colorplayer = GetComponent<Renderer>();
        colorplayer.material.SetColor("_Color", Color.green); // pongo el verde de base para que se vea bien 

    }

    // Update is called once per frame
    void Update()
    {
        if(cambio == true)
        {
            if (Input.GetButton("Color"))
            {

                colorplayer.material.SetColor("_Color", colorcamb);

            }
            else colorplayer.material.SetColor("_Color", Color.green);
        }
        else
        {
            if (Input.GetButton("Color"))
            {
                colorplayer.material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
                
            }
        }




    }
}