﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public WaypointController waypointControl;
    public float speed = 5;

	// Use this for initialization
	void Start () {



	}
	
	// Update is called once per frame  
	void Update () {

        float step = speed * Time.deltaTime;
        Vector3 currentWaypointPos = waypointControl.getCurrentWaypointPos();
        transform.position = Vector3.MoveTowards(transform.position, currentWaypointPos, step);
    }
}
