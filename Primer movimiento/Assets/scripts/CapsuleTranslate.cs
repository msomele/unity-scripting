﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleTranslate : MonoBehaviour {
    public float ScaleUnits = 5f;
    // Use this for initialization
    void Start () {
        transform.localScale = new Vector3(transform.localScale.x + ScaleUnits * Time.deltaTime,1 ,1 ); 
        Debug.Log("Scaling from Start Event");
    }
	
	// Update is called once per frame
	void Update () {
        transform.localScale = new Vector3(transform.localScale.x + ScaleUnits * Time.deltaTime, 1, 1);
        Debug.Log("Scaling from Start Event");
    }
}

