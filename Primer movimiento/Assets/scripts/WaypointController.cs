﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointController : MonoBehaviour {

    private Transform[] waypoints;
    private int currentWayPointIndex = 0;
    public PlayerController player;



	// Use this for initialization
	void Start () {

        waypoints = new Transform[transform.childCount];

        for (int i = 0; i<waypoints.Length; i++)
        {
            waypoints[i] = transform.GetChild(i);
        }

	}
	
	// Update is called once per frame
	void Update () {

        if (player.transform.position.Equals(waypoints[currentWayPointIndex].position))
        {
            currentWayPointIndex++;

            if (currentWayPointIndex >= waypoints.Length)
                currentWayPointIndex = 0;
        }


        }



    public Vector3 getCurrentWaypointPos()
    {
        return waypoints[currentWayPointIndex].position;
    }
}
