﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerControllerWithInputv2Rotar : MonoBehaviour
{

    public float speed;
    public float runSpeed;
    private float hInput, vInput;
    public bool cambio;
    public float velrot;

    // Use this for initialization
    void Start()
    {




    }

    // Update is called once per frame
    void Update()
    {

        hInput = Input.GetAxisRaw("Horizontal");
        vInput = Input.GetAxisRaw("Vertical");

        float step = speed;
        if (Input.GetButton("Run"))
        {
            step = runSpeed;
        }

        if (vInput != 0)
        {
            transform.Translate(vInput * Vector3.forward * Time.deltaTime * step);

        }
        if (cambio == true)
        {
            if (hInput != 0)
            {
                transform.Rotate(hInput * Vector3.right * Time.deltaTime * step * velrot);
            }
        }

        else
        {
            transform.Translate(hInput * Vector3.right * Time.deltaTime * step);
        }

    }
}
