﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject prefabToSpawn;
    GameObject prefabToSpawnClone;
    public Color colorNuevo;
    private Renderer rendNuevo;


    public int Contador = 0;
    public float secondsToCount;
    public float secondsCounter = 0;




    private void Start()
    {
        secondsToCount = Random.Range(0.5f, 3f);
    }
    

    // Update is called once per frame
    void Update () {


        secondsCounter += Time.deltaTime;
        if (secondsCounter >= secondsToCount)
        {

            secondsCounter = 0;
            Contador++;
            secondsToCount = Random.Range(0.5f, 3f);

            prefabToSpawnClone = Instantiate(prefabToSpawn, transform.position, Quaternion.identity) as GameObject;
            float scale = Random.Range(1, 10);
            prefabToSpawnClone.transform.localScale = new Vector3(scale, scale, scale);
            rendNuevo = prefabToSpawnClone.GetComponent<Renderer>();
            rendNuevo.material.SetColor("_Color", colorNuevo);
        //    SphereCollider cd = prefabToSpawnClone.GetComponent<SphereCollider>();
          //  cd.radius = scale/2; //test para ver porque el raycast no siempre da a las esferas pero si a los cubos
            Rigidbody rb = prefabToSpawnClone.GetComponent<Rigidbody>();
            rb.AddForce(Random.Range(0f, 10f) * Random.Range(0f,0.5f), Random.Range(0f, 10f) * Random.Range(0f, 0.5f), Random.Range(0f, 10f) * Random.Range(0f,0.5f), ForceMode.Impulse);




        }









        

        /*            GameObject ObjAux = Instantiate(objectToSpawn, transform.position, Quaternion.identity) as GameObject;
            float scale = Random.Range(1, 10);
            ObjAux.transform.localScale = new Vector3(scale, scale, scale);
            Rigidbody rb = ObjAux.GetComponent<Rigidbody>();
            rb.drag = Random.Range(0f, 0.1f);
            rb.mass = Random.Range(1, 25);
            rb.AddForce(new Vector3(1, 0, 0) * Random.Range(10, 150), ForceMode.Impulse);
            rb.AddForce(new Vector3(0, 1, 0) * Random.Range(10, 150), ForceMode.Impulse);
            rb.AddForce(new Vector3(0, 0, 1) * Random.Range(10, 150), ForceMode.Impulse);
            
        */


	}

}
