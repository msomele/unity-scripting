﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlDeMovimiento : MonoBehaviour {

    public GameObject aux;
    public float sensibilidad = 5;
    public float vel;
    float hInput;
    float vInput;
    float yaw;
    float pitch;


	// Use this for initialization
	void Start () {

       

	}
	
	// Update is called once per frame
	void Update () {
        Rigidbody rb = aux.GetComponent<Rigidbody>();

        //transform.Rotate( new Vector3 (-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0) * Time.deltaTime * sensibilidad);

        yaw += sensibilidad * Input.GetAxis("Mouse X");
        pitch -= sensibilidad * Input.GetAxis("Mouse Y");

        Vector3 CamMov = new Vector3(pitch, yaw, 0);
        transform.eulerAngles = (CamMov);

        hInput = Input.GetAxisRaw("Horizontal");
        vInput = Input.GetAxisRaw("Vertical");

        Vector3 movimientoPapa = (transform.forward * vInput + transform.right * hInput);
        rb.velocity = movimientoPapa * vel;
        


            
        
        



    }
}
