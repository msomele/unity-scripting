﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbital : MonoBehaviour {
    GameObject busqueda;
	public float radio = 4f;
    public float velocidad = 10f;
    public Vector3 pospla;
    public Vector3 posorb;
    public Vector3 posicion;
    
    // Use this for initialization
	void Start () {

        busqueda = GameObject.FindWithTag("Center"); //aqui "copio" la posicion del centro
        transform.position = (transform.position - busqueda.transform.position).normalized * radio + busqueda.transform.position;


    }
	
	// Update is called once per frame
	void Update () {
    
        pospla = busqueda.transform.position;

        
         transform.RotateAround(pospla, new Vector3(0, 1, 0), 100 * Time.deltaTime);
        posicion = (transform.position - busqueda.transform.position).normalized * radio + busqueda.transform.position;
        transform.position = Vector3.MoveTowards (transform.position, posicion, Time.deltaTime * velocidad);

        if(transform.position == busqueda.transform.position && radio != 0f)  
        {
            transform.position += new Vector3(1, 1, 1);
            /*esto lo añado porque si no cuando pones radio 0 y lo cambias a cualquier otro 
              no se mueve porque transform.position - busqueda.transform.position seria = 0
              y el resultado en posicion seria 0*radio + busqueda.transform.position
              y el movetowards siempre se moveria hacia su propia posicion*/

        }
    }
}
